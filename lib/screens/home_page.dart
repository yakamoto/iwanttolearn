import 'package:bghiteNe9rra/internationalization/app_localization.dart';
import 'package:bghiteNe9rra/providers/session_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

import '../const.dart';
import '../providers/user_provider.dart';
import '../theme.dart';
import '../widgets/custom_app_bar.dart';
import '../widgets/loading_page.dart';
import '../widgets/search_bar.dart';
import 'category_sessions.dart';
import 'template_layout.dart';

class HomePage extends StatelessWidget {
  HomePage();
  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return TemplateLayout(
      body: (userProvider.user == null || userProvider.waitingGoogleUser)
          ? LoadingPage()
          : buildPage(userProvider, context),
    );
  }

  Widget buildPage(UserProvider userProvider, BuildContext context) {
    return SafeArea(
      child: Container(
        child: Column(
          children: <Widget>[
            CustomAppBar(),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            GreetingWidget(),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            SearchBar(),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.035,
            ),
            HomeCategoriesBlock(),
          ],
        ),
      ),
    );
  }
}

class HomeCategoriesBlock extends StatelessWidget {
  const HomeCategoriesBlock({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.height * 0.07),
              topRight:
                  Radius.circular(MediaQuery.of(context).size.height * 0.07),
            ),
            boxShadow: [
              BoxShadow(
                blurRadius: 6,
                spreadRadius: 2,
                offset: Offset(0, -2),
                color: Colors.grey.withOpacity(0.7),
              )
            ]),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.075,
                  vertical: MediaQuery.of(context).size.height * 0.035),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    AppLocalizations.of(context).translate("categories"),
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/categories');
                    },
                    child: Text(
                      AppLocalizations.of(context).translate('see_all'),
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(color: CustomTheme.blue),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.03,
                  vertical: MediaQuery.of(context).size.height * 0.01,
                ),
                child: Consumer<SessionProvider>(
                    builder: (_, sessionProvider, __) {
                  return StaggeredGridView.countBuilder(
                    padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).size.height * 0.05,
                    ),
                    crossAxisCount: 2,
                    crossAxisSpacing: MediaQuery.of(context).size.width * 0.015,
                    mainAxisSpacing: MediaQuery.of(context).size.height * 0.01,
                    itemCount: sessionProvider.categoriesCount.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => CategorySessions(
                                category:
                                    sessionProvider.categoriesCount[index],
                              ),
                            ),
                          );
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              vertical:
                                  MediaQuery.of(context).size.height * 0.025,
                              horizontal:
                                  MediaQuery.of(context).size.width * 0.035),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                                MediaQuery.of(context).size.height * 0.03),
                            image: DecorationImage(
                              image: AssetImage(categories[index].img),
                              fit: BoxFit.fill,
                            ),
                          ),
                          height: index % 3 != 1
                              ? MediaQuery.of(context).size.height * 0.285
                              : MediaQuery.of(context).size.height * 0.35,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Hero(
                                tag: categories[index].name,
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate(categories[index].name),
                                  style: Theme.of(context).textTheme.headline6,
                                ),
                              ),
                              Text(
                                categories[index].sessionCount == 0
                                    ? AppLocalizations.of(context)
                                        .translate('no_sessions_available')
                                    : '${categories[index].sessionCount} ${AppLocalizations.of(context).translate('sessions_available')}',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    staggeredTileBuilder: (index) => StaggeredTile.fit(1),
                  );
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GreetingWidget extends StatelessWidget {
  const GreetingWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.075),
      child: Consumer<UserProvider>(builder: (_, userProvider, __) {
        return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
              userProvider.user.isAnonymous == false
                  ? '${AppLocalizations.of(context).translate('greeting1')} ${userProvider.user.displayName},'
                  : '${AppLocalizations.of(context).translate('greeting1')},',
              style: Theme.of(context).textTheme.headline5),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.009,
          ),
          Text(
            AppLocalizations.of(context).translate('greeting2'),
            style: Theme.of(context).textTheme.headline6,
          )
        ]);
      }),
    );
  }
}

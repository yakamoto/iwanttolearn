import 'package:bghiteNe9rra/internationalization/app_localization.dart';
import 'package:bghiteNe9rra/providers/user_provider.dart';
import 'package:bghiteNe9rra/widgets/call_to_action_button.dart';
import 'package:bghiteNe9rra/widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../theme.dart';
import 'template_layout.dart';

class BecameTutorForm extends StatelessWidget {
  const BecameTutorForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TemplateLayout(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              CustomAppBar(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.85,
                child: Column(
                  children: <Widget>[
                    Consumer<UserProvider>(builder: (_, userProvider, __) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TextField(
                            onChanged: (value) {
                              userProvider.profileLink = value;
                            },
                            cursorColor: CustomTheme.orange,
                            maxLength: 256,
                            decoration: InputDecoration(
                                errorText:
                                    userProvider.errors['profile_link'] != null
                                        ? ''
                                        : null,
                                labelText: AppLocalizations.of(context)
                                    .translate('profile_link'),
                                labelStyle: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .copyWith(
                                      color: CustomTheme.blue,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                hintText:
                                    'https://www.facebook.com/profile.id.XXXX'),
                          ),
                          userProvider.errors['profile_link'] != null
                              ? Text(
                                  '${AppLocalizations.of(context).translate(userProvider.errors['profile_link'])}',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      .copyWith(
                                        color: Theme.of(context).errorColor,
                                        fontSize: 16,
                                      ),
                                )
                              : SizedBox(
                                  height: 0,
                                ),
                        ],
                      );
                    }),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    Consumer<UserProvider>(builder: (_, userProvider, __) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TextField(
                            onChanged: (value) {
                              userProvider.description = value;
                            },
                            cursorColor: CustomTheme.orange,
                            maxLines: 5,
                            style:
                                Theme.of(context).textTheme.bodyText1.copyWith(
                                      fontSize: 18,
                                    ),
                            decoration: InputDecoration(
                              errorText:
                                  userProvider.errors['description'] != null
                                      ? ''
                                      : null,
                              border: OutlineInputBorder(),
                              labelText: AppLocalizations.of(context)
                                  .translate("form_tutor_description_label"),
                              labelStyle: Theme.of(context)
                                  .textTheme
                                  .subtitle1
                                  .copyWith(
                                    color: CustomTheme.blue,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                  ),
                              hintText: AppLocalizations.of(context)
                                  .translate("form_tutor_description_hint"),
                            ),
                          ),
                          userProvider.errors['description'] != null
                              ? Text(
                                  '${AppLocalizations.of(context).translate(userProvider.errors['description'])}',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      .copyWith(
                                          color: Theme.of(context).errorColor,
                                          fontSize: 16),
                                )
                              : SizedBox(
                                  height: 0,
                                ),
                        ],
                      );
                    }),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    Consumer<UserProvider>(
                        builder: (context, userProvider, __) {
                      return CallToActionButton(
                        text: AppLocalizations.of(context)
                            .translate("form_tutor_description_call_to_action"),
                        action: userProvider.isTutorValid
                            ? () {
                                userProvider.evolveUserToTutor();
                                Navigator.pushNamed(context, '/controller');
                              }
                            : null,
                      );
                    })
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

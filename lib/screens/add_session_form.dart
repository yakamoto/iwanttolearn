import 'package:bghiteNe9rra/internationalization/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../const.dart';
import '../providers/session_provider.dart';
import '../theme.dart';
import '../widgets/call_to_action_button.dart';
import '../widgets/custom_app_bar.dart';
import 'template_layout.dart';

class SessionForm extends StatelessWidget {
  const SessionForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SessionProvider sessionProvider = Provider.of<SessionProvider>(context);
    return TemplateLayout(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CustomAppBar(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              Container(
                margin: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.075),
                child: sessionProvider.fullSessions
                    ? Container(
                        height: MediaQuery.of(context).size.height * 0.89,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              AppLocalizations.of(context)
                                  .translate("add_session_error"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(
                                      color: Theme.of(context).errorColor),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.02,
                            ),
                            CallToActionButton(
                              text: AppLocalizations.of(context)
                                  .translate("add_session_error_action"),
                              action: () {
                                Navigator.pushNamed(context, '/home');
                              },
                            )
                          ],
                        ),
                      )
                    : buildSessionForm(context),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Column buildSessionForm(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).translate("session_form_title"),
          style: Theme.of(context).textTheme.headline5.copyWith(
                fontWeight: FontWeight.bold,
              ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.03,
        ),
        SessionTitleEntry(),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        ),
        CategoryEntry(),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        ),
        TimeDurationEntry(),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        ),
        SessionLinkEntry(),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        ),
        PlatFormEntry(),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        ),
        AudienceDescriptionEntry(),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        ),
        SessionDescriptionEntry(),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        ),
        TagsEntry(),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        ),
        Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
          return CallToActionButton(
              text: AppLocalizations.of(context)
                  .translate("session_form_callToAction"),
              action: () {
                sessionProvider.checkSessionValidity();
                if (sessionProvider.isSessionFormValid)
                  Navigator.pushNamed(context, '/controller');
              });
        }),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.07,
        ),
      ],
    );
  }
}

class TagsEntry extends StatelessWidget {
  const TagsEntry({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
            onChanged: (value) {
              sessionProvider.tags = value;
            },
            cursorColor: CustomTheme.orange,
            style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 18),
            decoration: InputDecoration(
              labelText: AppLocalizations.of(context)
                  .translate("session_form_tagsEntry_label"),
              hintText: AppLocalizations.of(context)
                  .translate("session_form_tagsEntry_hintText"),
              labelStyle: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: CustomTheme.blue,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['tags_error'] != null,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['tags_error'] != null,
            child: Text(
              '${AppLocalizations.of(context).translate(sessionProvider.errors['tags_error'])}',
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: Theme.of(context).errorColor,
                    fontSize: 16,
                  ),
            ),
          ),
        ],
      );
    });
  }
}

class SessionDescriptionEntry extends StatelessWidget {
  const SessionDescriptionEntry({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
            onChanged: (value) {
              sessionProvider.sessionDescription = value;
            },
            cursorColor: CustomTheme.orange,
            style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 18),
            maxLines: 5,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: AppLocalizations.of(context)
                  .translate("session_form_description_label"),
              labelStyle: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: CustomTheme.blue,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
            ),
          ),
          Visibility(
            visible:
                sessionProvider.errors['session_description_error'] != null,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
          ),
          Visibility(
            visible:
                sessionProvider.errors['session_description_error'] != null,
            child: Text(
              '${AppLocalizations.of(context).translate(sessionProvider.errors['session_description_error'])}',
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: Theme.of(context).errorColor,
                    fontSize: 16,
                  ),
            ),
          ),
        ],
      );
    });
  }
}

class AudienceDescriptionEntry extends StatelessWidget {
  const AudienceDescriptionEntry({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
            onChanged: (value) {
              sessionProvider.audienceDesciption = value;
            },
            cursorColor: CustomTheme.orange,
            maxLines: 3,
            style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 18),
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: AppLocalizations.of(context)
                  .translate("session_form_audienceDescription_label"),
              labelStyle: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: CustomTheme.blue,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
            ),
          ),
          Visibility(
            visible:
                sessionProvider.errors['audience_description_error'] != null,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
          ),
          Visibility(
            visible:
                sessionProvider.errors['audience_description_error'] != null,
            child: Text(
              '${AppLocalizations.of(context).translate(sessionProvider.errors['audience_description_error'])}',
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: Theme.of(context).errorColor,
                    fontSize: 16,
                  ),
            ),
          ),
        ],
      );
    });
  }
}

class PlatFormEntry extends StatelessWidget {
  const PlatFormEntry({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          DropdownButton(
            onChanged: (value) {
              sessionProvider.platform = value;
            },
            value: sessionProvider.platform,
            hint: Text(
              AppLocalizations.of(context)
                  .translate("session_form_platformEntry_hintText"),
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: CustomTheme.blue,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
            ),
            isExpanded: true,
            items: supportedPlatforms
                .map(
                  (e) => DropdownMenuItem(
                    value: e,
                    child: Text('${AppLocalizations.of(context).translate(e)}'),
                  ),
                )
                .toList(),
          ),
          Visibility(
            visible: sessionProvider.errors['platform_error'] != null,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['platform_error'] != null,
            child: Text(
              '${AppLocalizations.of(context).translate(sessionProvider.errors['platform_error'])}',
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: Theme.of(context).errorColor,
                    fontSize: 16,
                  ),
            ),
          ),
        ],
      );
    });
  }
}

class SessionLinkEntry extends StatelessWidget {
  const SessionLinkEntry({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
            onChanged: (value) {
              sessionProvider.sessionLink = value;
            },
            cursorColor: CustomTheme.orange,
            style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 18),
            decoration: InputDecoration(
              labelText: AppLocalizations.of(context)
                  .translate("session_form_linkEntry"),
              labelStyle: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: CustomTheme.blue,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['session_link_error'] != null,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['session_link_error'] != null,
            child: Text(
              '${AppLocalizations.of(context).translate(sessionProvider.errors['session_link_error'])}',
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: Theme.of(context).errorColor),
            ),
          ),
        ],
      );
    });
  }
}

class TimeDurationEntry extends StatelessWidget {
  TimeDurationEntry({
    Key key,
  }) : super(key: key);
  final TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
      _controller.text = sessionProvider.startTime;
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                flex: 11,
                child: TextField(
                  controller: _controller,
                  onTap: () async {
                    DateTime finalResult;
                    DateTime date = await showDatePicker(
                      context: context,
                      initialDate: sessionProvider.rawStartTime,
                      firstDate: DateTime.now(),
                      lastDate: DateTime.now().add(
                        Duration(days: 365),
                      ),
                    );
                    if (date != null) {
                      TimeOfDay time = await showTimePicker(
                        context: context,
                        initialTime: TimeOfDay.fromDateTime(date),
                      );
                      finalResult = DateTime(date.year, date.month, date.day,
                          time.hour, time.minute);
                      sessionProvider.startTime = finalResult;
                    }
                  },
                  readOnly: true,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(fontSize: 18),
                  cursorColor: CustomTheme.orange,
                  decoration: InputDecoration(
                    suffixIcon: Icon(
                      Icons.date_range,
                      size: 24,
                    ),
                    labelStyle: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: CustomTheme.blue,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                    labelText: AppLocalizations.of(context)
                        .translate("session_form_starttimeEntry"),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(),
              ),
              Expanded(
                flex: 4,
                child: TextField(
                  onChanged: (value) {
                    sessionProvider.duration = value;
                  },
                  keyboardType: TextInputType.number,
                  cursorColor: CustomTheme.orange,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(fontSize: 18),
                  decoration: InputDecoration(
                      hintText: AppLocalizations.of(context)
                          .translate("session_form_durationEntry_hintText"),
                      labelStyle:
                          Theme.of(context).textTheme.subtitle1.copyWith(
                                color: CustomTheme.blue,
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                      labelText: AppLocalizations.of(context)
                          .translate("session_form_durationEntry_Label")),
                ),
              ),
            ],
          ),
          Visibility(
            visible: sessionProvider.errors['start_time_error'] != null,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['start_time_error'] != null,
            child: Text(
              '${AppLocalizations.of(context).translate(sessionProvider.errors['start_time_error'])}',
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: Theme.of(context).errorColor,
                    fontSize: 16,
                  ),
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['duration_error'] != null,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['duration_error'] != null,
            child: Text(
              '${AppLocalizations.of(context).translate(sessionProvider.errors['duration_error'])}',
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: Theme.of(context).errorColor,
                    fontSize: 16,
                  ),
            ),
          ),
        ],
      );
    });
  }
}

class CategoryEntry extends StatelessWidget {
  const CategoryEntry({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          DropdownButton(
            onChanged: (value) {
              sessionProvider.category = value;
            },
            value: sessionProvider.category,
            hint: Text(
              AppLocalizations.of(context)
                  .translate("session_form_categoryEntry"),
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: CustomTheme.blue,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
            ),
            isExpanded: true,
            items: categories
                .map(
                  (e) => DropdownMenuItem(
                    value: e,
                    child: Text(
                        '${AppLocalizations.of(context).translate(e.name)}'),
                  ),
                )
                .toList(),
          ),
          Visibility(
            visible: sessionProvider.errors['session_category_error'] != null,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['session_category_error'] != null,
            child: Text(
              '${AppLocalizations.of(context).translate(sessionProvider.errors['session_category_error'])}',
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: Theme.of(context).errorColor, fontSize: 16),
            ),
          ),
        ],
      );
    });
  }
}

class SessionTitleEntry extends StatelessWidget {
  const SessionTitleEntry({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
            onChanged: (value) {
              sessionProvider.title = value;
            },
            cursorColor: CustomTheme.orange,
            style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 18),
            decoration: InputDecoration(
              labelText: AppLocalizations.of(context)
                  .translate("session_form_titleEntry"),
              labelStyle: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: CustomTheme.blue,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['title_error'] != null,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
          ),
          Visibility(
            visible: sessionProvider.errors['title_error'] != null,
            child: Text(
              "${AppLocalizations.of(context).translate(sessionProvider.errors['title_error'])}",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: Theme.of(context).errorColor, fontSize: 16),
            ),
          ),
        ],
      );
    });
  }
}

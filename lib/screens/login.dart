import 'package:bghiteNe9rra/internationalization/app_localization.dart';
import 'package:bghiteNe9rra/providers/user_provider.dart';
import 'package:bghiteNe9rra/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            right: 0,
            top: 0,
            child: Container(
                height: MediaQuery.of(context).size.height * 0.5,
                child: Image.asset('assets/top.png')),
          ),
          buildTeacherImageAndDescription(context),
          Positioned(
            bottom: 0,
            left: 0,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.2,
              child: Image.asset('assets/bot.png'),
            ),
          ),
          buildLoginTitle(context),
          buildSignInRow(context),
        ],
      ),
    );
  }

  Future<void> _askToLogin(
      BuildContext context, UserProvider userProvider) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            children: <Widget>[
              SimpleDialogOption(
                child: RowIconButton(
                  text: AppLocalizations.of(context)
                      .translate("login_signin_google"),
                  image: Image.asset('assets/google.png'),
                  onPressed: () {
                    userProvider.gooSignIn();
                    Navigator.pushNamed(context, '/home');
                  },
                ),
              ),
              SimpleDialogOption(
                child: RowIconButton(
                  text: AppLocalizations.of(context)
                      .translate("login_signin_anonymously"),
                  image: Image.asset('assets/anonymous.png'),
                  onPressed: () {
                    userProvider.signInAnonym();
                    Navigator.pushNamed(context, '/home');
                  },
                ),
              )
            ],
          );
        });
  }

  Positioned buildSignInRow(BuildContext context) {
    return Positioned(
      bottom: MediaQuery.of(context).size.height * 0.1,
      right: MediaQuery.of(context).size.width * 0.1,
      child: Row(
        children: <Widget>[
          Text(
            AppLocalizations.of(context).translate("login_sign_in"),
            style: Theme.of(context).textTheme.headline4,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.1,
          ),
          Consumer<UserProvider>(builder: (_, userProvider, __) {
            return GestureDetector(
              onTap: () {
                _askToLogin(context, userProvider);
              },
              child: Container(
                width: MediaQuery.of(context).size.height * 0.1,
                height: MediaQuery.of(context).size.height * 0.1,
                decoration: BoxDecoration(
                    color: CustomTheme.grey,
                    borderRadius: BorderRadius.circular(100),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.5),
                        blurRadius: 3,
                        spreadRadius: 1,
                        offset: Offset(1, 1),
                      )
                    ]),
                child: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.white,
                ),
              ),
            );
          }),
        ],
      ),
    );
  }

  Positioned buildTeacherImageAndDescription(BuildContext context) {
    return Positioned(
      top: MediaQuery.of(context).size.height * 0.35,
      left: MediaQuery.of(context).size.width * 0.025,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.35,
        width: MediaQuery.of(context).size.width * 0.95,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Align(
                alignment: Alignment.topLeft,
                child: Image.asset('assets/teacher.png')),
            Positioned(
              bottom: MediaQuery.of(context).size.height * 0.09,
              right: 10,
              child: Text(
                AppLocalizations.of(context).translate("login_slogant"),
                style: Theme.of(context)
                    .textTheme
                    .subtitle1
                    .copyWith(fontSize: 16),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Positioned buildLoginTitle(BuildContext context) {
    return Positioned(
      top: MediaQuery.of(context).size.height * 0.15,
      left: MediaQuery.of(context).size.width * 0.07,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).translate("login_greeting1"),
            style: Theme.of(context).textTheme.headline5,
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.01,
          ),
          Text(
            AppLocalizations.of(context).translate("login_greeting2"),
            style: Theme.of(context).textTheme.headline5,
          ),
        ],
      ),
    );
  }

  Widget buildLoginForm(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Consumer<UserProvider>(builder: (_, userProvider, __) {
            return RowIconButton(
              text: AppLocalizations.of(context).translate("login_sign_in"),
              image: Image.asset('assets/google.png'),
              onPressed: () {
                userProvider.gooSignIn();
              },
            );
          }),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.02,
          ),
          Consumer<UserProvider>(builder: (_, userProvider, __) {
            return RowIconButton(
              text: AppLocalizations.of(context)
                  .translate("login_signin_anonymously"),
              image: Image.asset('assets/anonymous.png'),
              onPressed: () {
                userProvider.signInAnonym();
              },
            );
          })
        ],
      ),
    );
  }
}

class RowIconButton extends StatelessWidget {
  final String text;
  final Widget image;
  final Function onPressed;
  RowIconButton(
      {@required this.text, @required this.image, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.white,
      onPressed: onPressed,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.07,
        // width: MediaQuery.of(context).size.height * 0.3,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: 32,
              child: image,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.03,
            ),
            Text(
              text,
              style: TextStyle(fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/user_provider.dart';
import 'home_page.dart';
import 'login.dart';

class Controller extends StatelessWidget {
  const Controller({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    if (userProvider.user == null) {
      return LoginScreen();
    } else
      return HomePage();
  }
}

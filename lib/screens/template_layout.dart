import 'package:bghiteNe9rra/internationalization/app_localization.dart';
import 'package:bghiteNe9rra/screens/select_language_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/session_provider.dart';
import '../providers/user_provider.dart';
import '../theme.dart';
import 'add_session_form.dart';
import 'became_teacher_form.dart';
import 'become_tutor.dart';
import 'tutor_sessions.dart';

class TemplateLayout extends StatelessWidget {
  final Widget body;
  TemplateLayout({@required this.body});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: CustomDrawer(),
      backgroundColor: Colors.white,
      body: body,
    );
  }
}

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    SessionProvider sessionProvider =
        Provider.of<SessionProvider>(context, listen: false);
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/drawer.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.03),
          ListTile(
            onTap: () {
              Navigator.pushNamed(context, '/controller');
            },
            title: Text(
              AppLocalizations.of(context).translate('drawer_home'),
              style: Theme.of(context).textTheme.subtitle1.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            leading: Icon(
              Icons.home,
              size: MediaQuery.of(context).size.height * 0.04,
            ),
          ),
          userProvider.tutor != null
              ? ListTile(
                  onTap: () {
                    sessionProvider.checkQuota();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => SessionForm()));
                  },
                  title: Text(
                    AppLocalizations.of(context)
                        .translate("drawer_add_session"),
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  leading: Icon(
                    Icons.add,
                    color: CustomTheme.blue,
                    size: MediaQuery.of(context).size.height * 0.04,
                  ),
                )
              : SizedBox(
                  height: 0,
                ),
          userProvider.tutor != null
              ? ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => TutorSession(),
                      ),
                    );
                  },
                  title: Text(
                    AppLocalizations.of(context)
                        .translate("drawer_my_sessions"),
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  leading: Icon(
                    Icons.book,
                    color: CustomTheme.blue,
                    size: MediaQuery.of(context).size.height * 0.04,
                  ),
                )
              : SizedBox(
                  height: 0,
                ),
          userProvider.user?.isAnonymous
              ? ListTile(
                  onTap: () {
                    userProvider.gooSignIn();
                    Navigator.pushNamed(context, '/controller');
                  },
                  title: Text(
                    AppLocalizations.of(context)
                        .translate("drawer_google_sign_in"),
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  leading: Image(
                    width: MediaQuery.of(context).size.height * 0.04,
                    height: MediaQuery.of(context).size.height * 0.04,
                    image: AssetImage(
                      'assets/google.png',
                    ),
                  ),
                )
              : SizedBox(
                  height: 0,
                ),
          (userProvider.user?.isAnonymous || userProvider.tutor != null)
              ? SizedBox(
                  height: 0,
                )
              : ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => BecameTutorForm(),
                      ),
                    );
                  },
                  title: Text(
                    AppLocalizations.of(context)
                        .translate("drawer_became_tutor"),
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  leading: Icon(
                    Icons.person_add,
                    color: CustomTheme.blue,
                    size: MediaQuery.of(context).size.height * 0.04,
                  ),
                ),
          (userProvider.tutor == null)
              ? ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => BecameTutor(),
                      ),
                    );
                  },
                  title: Text(
                    AppLocalizations.of(context)
                        .translate("drawer_how_became_tutor"),
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  leading: Icon(
                    Icons.help,
                    color: CustomTheme.blue,
                    size: MediaQuery.of(context).size.height * 0.04,
                  ),
                )
              : SizedBox(
                  height: 0,
                ),
          ListTile(
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => SelectLanguage()));
            },
            title: Text(
              AppLocalizations.of(context).translate("drawer_select_language"),
              style: Theme.of(context).textTheme.subtitle1.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            leading: Icon(
              Icons.language,
              color: CustomTheme.blue,
              size: MediaQuery.of(context).size.height * 0.04,
            ),
          ),
          ListTile(
            onTap: () {
              userProvider.allLogOut();
              userProvider.user = null;

              Navigator.pushNamed(context, '/controller');
            },
            title: Text(
              AppLocalizations.of(context).translate("drawer_log_out"),
              style: Theme.of(context).textTheme.subtitle1.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            leading: Icon(
              Icons.close,
              color: CustomTheme.orange,
              size: MediaQuery.of(context).size.height * 0.04,
            ),
          ),
        ],
      ),
    );
  }
}

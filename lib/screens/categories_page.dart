import 'package:bghiteNe9rra/const.dart';
import 'package:bghiteNe9rra/internationalization/app_localization.dart';
import 'package:bghiteNe9rra/models/session.dart';
import 'package:bghiteNe9rra/providers/session_provider.dart';
import 'package:bghiteNe9rra/screens/category_sessions.dart';
import 'package:bghiteNe9rra/theme.dart';
import 'package:bghiteNe9rra/widgets/search_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/custom_app_bar.dart';
import '../widgets/loading_page.dart';
import 'template_layout.dart';

class CategoriesPage extends StatelessWidget {
  CategoriesPage();
  @override
  Widget build(BuildContext context) {
    SessionProvider sessionProvider = Provider.of<SessionProvider>(context);
    return TemplateLayout(
      body: sessionProvider.categoriesCount == null
          ? LoadingPage()
          : buildPage(sessionProvider, context),
    );
  }

  buildPage(SessionProvider sessionProvider, BuildContext context) {
    return SafeArea(
      child: Container(
        child: Column(children: <Widget>[
          CustomAppBar(),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.02,
          ),
          SearchBar(),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.02,
          ),
          Expanded(
            child: ListView.separated(
              itemCount: sessionProvider.categoriesCount.length,
              itemBuilder: (_, index) {
                List<Session> validSessions = sessionProvider.allSessions
                    .where((session) =>
                        session.category.name ==
                        sessionProvider.categoriesCount[index].name)
                    .toList();
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => CategorySessions(
                          category: categories[index],
                        ),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width * 0.032),
                    height: MediaQuery.of(context).size.height * 0.19,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.3),
                          spreadRadius: 2,
                          blurRadius: 7,
                          offset: Offset(
                            0,
                            2,
                          ),
                        )
                      ],
                    ),
                    child: Row(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(15),
                              bottomLeft: Radius.circular(15),
                            ),
                            image: DecorationImage(
                                image: AssetImage(categories[index].img),
                                fit: BoxFit.fill),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.02,
                            left: MediaQuery.of(context).size.height * 0.02,
                            right: MediaQuery.of(context).size.height * 0.02,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Hero(
                                tag: categories[index].name,
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate(categories[index].name),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6
                                      .copyWith(fontWeight: FontWeight.w600),
                                ),
                              ),
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.005,
                              ),
                              Text(
                                '${AppLocalizations.of(context).translate("categories_page_sessions")}: ${validSessions.length}',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(
                                        fontWeight: FontWeight.w600,
                                        color: CustomTheme.blue),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
              padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).size.height * 0.05,
              ),
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                );
              },
            ),
          )
        ]),
      ),
    );
  }
}

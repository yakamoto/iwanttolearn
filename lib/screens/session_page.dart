import 'package:bghiteNe9rra/internationalization/app_localization.dart';
import 'package:bghiteNe9rra/models/session.dart';
import 'package:bghiteNe9rra/screens/template_layout.dart';
import 'package:bghiteNe9rra/widgets/call_to_action_button.dart';
import 'package:bghiteNe9rra/widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../theme.dart';

class SessionPage extends StatelessWidget {
  final Session session;
  SessionPage({this.session});

  @override
  Widget build(BuildContext context) {
    return TemplateLayout(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              CustomAppBar(),
              Container(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.height * 0.015),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.03,
                    ),
                    TitleContainer(session: session),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.03,
                    ),
                    TimeInformationContainer(session: session),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.03,
                    ),
                    Row(
                      textBaseline: TextBaseline.alphabetic,
                      children: <Widget>[
                        Text(
                          AppLocalizations.of(context)
                              .translate('session_tags'),
                          style: Theme.of(context).textTheme.subtitle1.copyWith(
                                color: CustomTheme.blue,
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                        ),
                        ...session.tags.map((tag) {
                          String toShow;
                          if (tag == session.tags[session.tags.length - 1])
                            toShow = '$tag';
                          else
                            toShow = '$tag ,';
                          return Text(
                            toShow,
                            style:
                                Theme.of(context).textTheme.bodyText1.copyWith(
                                      fontSize: 16,
                                    ),
                          );
                        }).toList(),
                      ],
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.03,
                    ),
                    SessionAudienceContainer(session: session),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.03,
                    ),
                    SessionDescription(session: session),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.03,
                    ),
                    CallToActionButton(
                      text: AppLocalizations.of(context)
                          .translate("session_call_to_action"),
                      action: () {
                        print(session.link);
                      },
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.05,
                    ),
                    CustomDivider(),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.03,
                    ),
                    TutorInformationContainer(session: session),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TutorInformationContainer extends StatelessWidget {
  const TutorInformationContainer({
    Key key,
    @required this.session,
  }) : super(key: key);

  final Session session;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          '${AppLocalizations.of(context).translate("session_about_tutor")} (${session.tutor.displayName})',
          style: Theme.of(context).textTheme.subtitle1.copyWith(
                fontWeight: FontWeight.bold,
                color: CustomTheme.blue,
                fontSize: 20,
              ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.01,
        ),
        Text(
          '${session.tutor.description}',
          style: Theme.of(context).textTheme.bodyText1.copyWith(
                fontSize: 18,
              ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.03,
        ),
        CallToActionButton(
            text:
                '${AppLocalizations.of(context).translate("session_consult_ptofile")}',
            action: () {
              print(session.tutor.displayName);
            }),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.15,
        ),
      ],
    );
  }
}

class CustomDivider extends StatelessWidget {
  const CustomDivider({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width * 0.75,
        child: Divider(
          color: CustomTheme.orange,
          thickness: 2,
        ),
      ),
    );
  }
}

class TimeInformationContainer extends StatelessWidget {
  const TimeInformationContainer({
    Key key,
    @required this.session,
  }) : super(key: key);

  final Session session;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: AppLocalizations.of(context).translate("session_start_time"),
            style: Theme.of(context).textTheme.subtitle1.copyWith(
                  color: CustomTheme.blue,
                  fontWeight: FontWeight.bold,
                ),
          ),
          TextSpan(
            text:
                '${DateFormat.yMMMMd().add_jm().format(session.sessionStart)} ',
            style: Theme.of(context).textTheme.subtitle2,
          ),
          TextSpan(
            text: '/ ',
            style: Theme.of(context).textTheme.subtitle2.copyWith(
                  color: CustomTheme.orange,
                ),
          ),
          TextSpan(
            text: AppLocalizations.of(context).translate("session_duration"),
            style: Theme.of(context).textTheme.subtitle2.copyWith(
                  color: CustomTheme.blue,
                  fontWeight: FontWeight.bold,
                ),
          ),
          TextSpan(
            text:
                '${session.duration.inMinutes} ${AppLocalizations.of(context).translate("session_duration_unit")}',
            style: Theme.of(context).textTheme.subtitle2.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
        ],
      ),
    );
  }
}

class SessionAudienceContainer extends StatelessWidget {
  const SessionAudienceContainer({
    Key key,
    @required this.session,
  }) : super(key: key);

  final Session session;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).translate("session_audience"),
          style: Theme.of(context).textTheme.subtitle1.copyWith(
                color: CustomTheme.blue,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.01),
        Text(
          session.audienceDescription,
          style: Theme.of(context).textTheme.bodyText1.copyWith(
                fontSize: 18,
              ),
        ),
      ],
    );
  }
}

class SessionDescription extends StatelessWidget {
  const SessionDescription({
    Key key,
    @required this.session,
  }) : super(key: key);

  final Session session;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).translate("session_description"),
          style: Theme.of(context).textTheme.subtitle1.copyWith(
                color: CustomTheme.blue,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.01,
        ),
        Container(
          child: Text(
            session.longDescription,
            style: Theme.of(context).textTheme.bodyText1.copyWith(
                  fontSize: 18,
                ),
          ),
        )
      ],
    );
  }
}

class TitleContainer extends StatelessWidget {
  const TitleContainer({
    Key key,
    @required this.session,
  }) : super(key: key);
  final Session session;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Hero(
        tag: session.title,
        child: Text(
          session.title,
          style: Theme.of(context).textTheme.headline4,
          maxLines: 2,
        ),
      ),
    );
  }
}

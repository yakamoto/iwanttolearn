import 'package:bghiteNe9rra/models/session.dart';
import 'package:bghiteNe9rra/providers/session_provider.dart';
import 'package:bghiteNe9rra/screens/session_page.dart';
import 'package:bghiteNe9rra/screens/template_layout.dart';
import 'package:bghiteNe9rra/theme.dart';
import 'package:bghiteNe9rra/widgets/custom_app_bar.dart';
import 'package:bghiteNe9rra/widgets/search_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SearchResult extends StatelessWidget {
  final List<Session> sessions;
  SearchResult({@required this.sessions});

  @override
  Widget build(BuildContext context) {
    return TemplateLayout(
      body: SafeArea(
        child: Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
          List<Session> validSessions = sessions;
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CustomAppBar(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              SearchBar(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              buildSessionList(context, validSessions),
            ],
          );
        }),
      ),
    );
  }

  Widget buildSessionList(BuildContext context, List<Session> validSessions) {
    return Expanded(
      child: ListView.separated(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).size.height * 0.05,
          ),
          itemBuilder: (_, index) => GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) =>
                          SessionPage(session: validSessions[index]),
                    ),
                  );
                },
                child: Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.075),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(
                          MediaQuery.of(context).size.height * 0.02),
                      bottomLeft: Radius.circular(
                          MediaQuery.of(context).size.height * 0.02),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * 0.75,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Hero(
                              tag: validSessions[index].title,
                              child: Text(
                                validSessions[index].title,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6
                                    .copyWith(fontWeight: FontWeight.w600),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  'by ',
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle2
                                      .copyWith(
                                        color: CustomTheme.blue,
                                      ),
                                ),
                                Text(
                                  validSessions[index].tutor.displayName,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      .copyWith(),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  validSessions[index].platform,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      .copyWith(fontWeight: FontWeight.w600),
                                ),
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.01,
                                ),
                                Text(
                                  ' / ',
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      .copyWith(color: CustomTheme.blue),
                                ),
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.01,
                                ),
                                Text(
                                  DateFormat.yMMMMd().add_jm().format(
                                      validSessions[index].sessionStart),
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      .copyWith(fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.65,
                              child: Text(
                                validSessions[index].longDescription,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(
                                      fontSize: 18,
                                    ),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        child: Icon(
                          Icons.arrow_forward,
                          size: MediaQuery.of(context).size.width * 0.09,
                          color: CustomTheme.blue,
                        ),
                      )
                    ],
                  ),
                ),
              ),
          separatorBuilder: (_, index) => Container(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.29),
                child: Divider(
                  thickness: 2,
                  color: CustomTheme.orange,
                ),
              ),
          itemCount: validSessions.length),
    );
  }
}

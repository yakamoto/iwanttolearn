import 'package:bghiteNe9rra/internationalization/app_localization.dart';
import 'package:bghiteNe9rra/screens/template_layout.dart';
import 'package:bghiteNe9rra/widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';

import '../theme.dart';

class BecameTutor extends StatelessWidget {
  const BecameTutor();

  @override
  Widget build(BuildContext context) {
    return TemplateLayout(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            CustomAppBar(),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            Container(
              width: MediaQuery.of(context).size.width * 0.95,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      'How to became a Tutor',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  NumeredPoint(
                    number: '1',
                    description: AppLocalizations.of(context)
                        .translate("how_became_tutor_1"),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  NumeredPoint(
                    number: '2',
                    description: AppLocalizations.of(context)
                        .translate("how_became_tutor_2"),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  NumeredPoint(
                    number: '3',
                    description: AppLocalizations.of(context)
                        .translate("how_became_tutor_3"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NumeredPoint extends StatelessWidget {
  const NumeredPoint({
    Key key,
    this.number,
    this.description,
  }) : super(key: key);
  final String number;
  final String description;
  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.justify,
      text: TextSpan(
        children: [
          TextSpan(
              text: '$number- ',
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: CustomTheme.orange,
                    fontSize: 22,
                    fontWeight: FontWeight.w900,
                  )),
          TextSpan(
            text: description,
            style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 19),
          ),
        ],
      ),
    );
  }
}

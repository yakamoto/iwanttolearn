import 'package:bghiteNe9rra/internationalization/app_localization.dart';
import 'package:bghiteNe9rra/providers/user_provider.dart';
import 'package:bghiteNe9rra/screens/template_layout.dart';
import 'package:bghiteNe9rra/widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../theme.dart';

class SelectLanguage extends StatelessWidget {
  const SelectLanguage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TemplateLayout(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            CustomAppBar(),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.75,
              height: MediaQuery.of(context).size.height * 0.3,
              child: Consumer<UserProvider>(builder: (_, userProvider, __) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    MaterialButton(
                      onPressed: () {
                        userProvider.appLocale = 'ar';
                        Navigator.pushNamed(context, '/home');
                      },
                      child: Text(
                        AppLocalizations.of(context).translate('Arabic'),
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(color: CustomTheme.blue),
                      ),
                    ),
                    MaterialButton(
                      onPressed: () {
                        userProvider.appLocale = 'en';
                        Navigator.pushNamed(context, '/home');
                      },
                      child: Text(
                        AppLocalizations.of(context).translate('English'),
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(color: CustomTheme.blue),
                      ),
                    ),
                    MaterialButton(
                      onPressed: () {
                        userProvider.appLocale = 'fr';
                        Navigator.pushNamed(context, '/home');
                      },
                      child: Text(
                        AppLocalizations.of(context).translate('French'),
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(color: CustomTheme.blue),
                      ),
                    ),
                  ],
                );
              }),
            )
          ],
        ),
      ),
    );
  }
}

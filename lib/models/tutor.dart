import 'package:bghiteNe9rra/models/user.dart';

class Tutor {
  String uid;
  String email;
  String displayName;
  String profileLink;
  String description;

  Tutor({this.displayName, this.description});

  Tutor.fromUser(User user)
      : uid = user.uid,
        email = user.email,
        displayName = user.displayName;
  String toString() {
    return 'Email: $email,\n DisplayName: $displayName,\n profileLink: $profileLink,\n Description: $description\n';
  }

  Map<String, dynamic> toJson() {
    return {
      'uid': uid,
      'email': email,
      'displayName': displayName,
      'profileLink': profileLink,
      'description': description,
    };
  }

  Tutor.fromJson(Map<String, dynamic> data) {
    uid = data['uid'] == null ? null : data['uid'];
    profileLink = data['profileLink'];
    displayName = data['displayName'];
    description = data['description'];
    email = data['email'];
  }
}

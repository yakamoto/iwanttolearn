import 'package:firebase_auth/firebase_auth.dart';

class User {
  bool isAnonymous;
  String displayName;
  String uid;
  String email;

  User();

  User.fromFireBaseUser(FirebaseUser user)
      : uid = user.uid,
        isAnonymous = user.isAnonymous,
        email = user.email,
        displayName = user.displayName;

  String toString() {
    return 'User: <$uid $displayName  isAnonymous: $isAnonymous>';
  }
}

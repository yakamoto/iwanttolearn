import 'package:bghiteNe9rra/models/category.dart';
import 'package:intl/intl.dart';

import 'tutor.dart';

class Session {
  String id;
  Category category;
  Tutor tutor;
  String platform;
  String title;
  String audienceDescription;
  String longDescription;
  List<String> tags;
  DateTime sessionStart;
  Duration duration;
  String link;

  Session({
    this.category,
    this.tutor,
    this.title,
    this.platform,
    this.sessionStart,
    this.duration,
    this.tags,
    this.link,
    this.audienceDescription,
    this.longDescription,
  });

  String toString() {
    return """
        'tutor': ${tutor.uid},
        'title': $title,
        'category': ${category.name},
        'sessionStart': ${DateFormat('d-m-y').add_jm().format(sessionStart)},
        'duration': ${duration.inMinutes},
        'tags': $tags,
        'link': $link,
        'platform':$platform,
        'audienceDescription': $audienceDescription,
        'longDescription': $longDescription,""";
  }

  Map<String, dynamic> toJson(String uid) => {
        'tutor': tutor.toJson(),
        'title': title,
        'category': category.name,
        'sessionStart': sessionStart,
        'duration': duration.inMinutes,
        'tags': tags.map((e) => e.toLowerCase()).toList(),
        'link': link,
        'platform': platform,
        'audienceDescription': audienceDescription,
        'longDescription': longDescription,
      };
  Session.fromJson(Map<String, dynamic> data)
      : tutor = Tutor.fromJson(data['tutor']),
        title = data['title'],
        category = Category(name: data['category']),
        sessionStart = data['sessionStart'].toDate(),
        duration = Duration(minutes: data['duration']),
        tags = data['tags'].cast<String>(),
        link = data['link'],
        platform = data['platform'],
        audienceDescription = data['audienceDescription'],
        longDescription = data['longDescription'];
}

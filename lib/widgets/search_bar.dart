import 'package:bghiteNe9rra/internationalization/app_localization.dart';
import 'package:bghiteNe9rra/models/session.dart';
import 'package:bghiteNe9rra/providers/session_provider.dart';
import 'package:bghiteNe9rra/screens/search_results.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../theme.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.85,
      height: MediaQuery.of(context).size.width * 0.1,
      child: Material(
        elevation: 5,
        child: Consumer<SessionProvider>(builder: (_, sessionProvider, __) {
          return TextField(
            onSubmitted: (value) {
              List<Session> searchResult = sessionProvider.searchByTag(value);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => SearchResult(sessions: searchResult)));
            },
            textAlignVertical: TextAlignVertical.center,
            cursorColor: CustomTheme.grey,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height * 0.02),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.horizontal()),
              prefixIcon: Icon(
                Icons.search,
                size: MediaQuery.of(context).size.width * 0.065,
              ),
              hintText: AppLocalizations.of(context).translate('search_by_tag'),
            ),
          );
        }),
      ),
    );
  }
}

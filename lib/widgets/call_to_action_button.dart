import 'package:flutter/material.dart';

import '../theme.dart';

class CallToActionButton extends StatelessWidget {
  const CallToActionButton({
    Key key,
    @required this.text,
    @required this.action,
  }) : super(key: key);

  final String text;
  final Function action;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: OutlineButton(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.07,
            vertical: MediaQuery.of(context).size.height * 0.015),
        textColor: CustomTheme.grey,
        borderSide: BorderSide(color: CustomTheme.orange),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        onPressed: action,
        child: Text(
          text,
          style: Theme.of(context).textTheme.headline6,
        ),
      ),
    );
  }
}

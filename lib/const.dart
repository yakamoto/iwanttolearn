import 'models/category.dart';
import 'models/session.dart';
import 'models/tutor.dart';

List<String> supportedPlatforms = ['Facebook', 'Instagram'];

List<String> categoriesNames = [
  'English',
  'Informatics',
  'French',
  'Islamic Education',
  'Arabic',
  'Mathematics',
  'Physics',
  'Life and Earth Sciences',
  'Science of engineering',
  'Economy',
  'Social studies',
  'Others'
];

List<Category> categories = categoriesNames
    .asMap()
    .map((key, value) => MapEntry(
          key,
          Category(
              id: key.toString(),
              img: 'assets/catagories_images/math.png',
              name: value),
        ))
    .values
    .toList()
    .cast<Category>();

Tutor tutor1 = Tutor(
    displayName: 'Tutor 1',
    description: 'This is a description of a tutor of the warzonegame');
Tutor tutor2 = Tutor(
    displayName: 'Tutor 2',
    description: 'This is a description of a tutor of the warzonegame');
Tutor tutor3 = Tutor(
    displayName: 'Tutor 3',
    description: 'This is a description of a tutor of the warzonegame');
DateTime daba = DateTime.now();
List<Session> sessions = [
  Session(
    tutor: tutor1,
    title: 'Session 1',
    category: categories[0],
    platform: supportedPlatforms[0],
    sessionStart: daba,
    duration: Duration(hours: 1, minutes: 30),
    tags: ['Tag 1', 'Tag 2', 'Tag 3'],
    link: 'https://facebook.com/id=asdasda',
    audienceDescription:
        'The audience for this is specific to people who are ....',
    longDescription:
        'We will cover letters and matters of the importance of how will the power of the potentiel intellect...',
  ),
  Session(
    category: categories[0],
    tutor: tutor1,
    title: 'Session 2',
    platform: supportedPlatforms[0],
    sessionStart: daba,
    duration: Duration(hours: 1, minutes: 30),
    tags: ['Tag 1', 'Tag 2', 'Tag 3'],
    link: 'https://facebook.com/id=asdasda',
    audienceDescription:
        'The audience for this is specific to people who are ....',
    longDescription:
        'We will cover letters and matters of the importance of how will the power of the potentiel intellect...',
  ),
  Session(
    tutor: tutor1,
    title: 'Session 3',
    category: categories[0],
    platform: supportedPlatforms[1],
    sessionStart: daba,
    duration: Duration(hours: 1, minutes: 30),
    tags: ['Tag 1', 'Tag 2', 'Tag 3'],
    link: 'https://facebook.com/id=asdasda',
    audienceDescription:
        'The audience for this is specific to people who are ....',
    longDescription:
        'We will cover letters and matters of the importance of how will the power of the potentiel intellect...',
  ),
  Session(
    tutor: tutor1,
    title: 'Session 4',
    category: categories[1],
    platform: supportedPlatforms[0],
    sessionStart: daba,
    duration: Duration(hours: 1, minutes: 30),
    tags: ['Tag 1', 'Tag 2', 'Tag 3'],
    link: 'https://facebook.com/id=asdasda',
    audienceDescription:
        'The audience for this is specific to people who are ....',
    longDescription:
        'We will cover letters and matters of the importance of how will the power of the potentiel intellect...',
  ),
  Session(
    tutor: tutor1,
    title: 'Session 5',
    category: categories[1],
    platform: supportedPlatforms[1],
    sessionStart: daba,
    duration: Duration(hours: 1, minutes: 30),
    tags: ['Tag 1', 'Tag 2', 'Tag 3'],
    link: 'https://facebook.com/id=asdasda',
    audienceDescription:
        'The audience for this is specific to people who are ....',
    longDescription:
        'We will cover letters and matters of the importance of how will the power of the potentiel intellect...',
  ),
  Session(
    tutor: tutor1,
    title: 'Session 6',
    category: categories[2],
    platform: supportedPlatforms[0],
    sessionStart: daba,
    duration: Duration(hours: 1, minutes: 30),
    tags: ['Tag 1', 'Tag 2', 'Tag 3'],
    link: 'https://facebook.com/id=asdasda',
    audienceDescription:
        'The audience for this is specific to people who are ....',
    longDescription:
        'We will cover letters and matters of the importance of how will the power of the potentiel intellect...',
  ),
  Session(
    tutor: tutor1,
    title: 'Session 7',
    category: categories[2],
    platform: supportedPlatforms[1],
    sessionStart: daba,
    duration: Duration(hours: 1, minutes: 30),
    tags: ['Tag 1', 'Tag 2', 'Tag 3'],
    link: 'https://facebook.com/id=asdasda',
    audienceDescription:
        'The audience for this is specific to people who are ....',
    longDescription:
        'We will cover letters and matters of the importance of how will the power of the potentiel intellect...',
  ),
];

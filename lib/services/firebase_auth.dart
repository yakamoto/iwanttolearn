import 'package:bghiteNe9rra/models/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthService {
  final GoogleSignIn gSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<User> signInAnonymously() async {
    try {
      AuthResult response = await _auth.signInAnonymously();
      print(response.user);
      FirebaseUser user = response.user;
      User finalUser = User.fromFireBaseUser(user);
      return finalUser;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<User> googleSignIn() async {
    try {
      GoogleSignInAccount googleSignInAccount = await gSignIn.signIn();
      GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;
      AuthCredential authCredentials = GoogleAuthProvider.getCredential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);
      AuthResult result = await _auth.signInWithCredential(authCredentials);
      return User.fromFireBaseUser(result.user);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<void> logout() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<void> googleLogout() async {
    try {
      await _auth.signOut();
      return await gSignIn.signOut();
    } catch (e) {
      print(e);
      return null;
    }
  }
}

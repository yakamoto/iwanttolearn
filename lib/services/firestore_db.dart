import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/session.dart';
import '../models/tutor.dart';
import '../models/user.dart';

class FirestoreDataBase {
  final CollectionReference tutors = Firestore.instance.collection('tutors');
  final CollectionReference sessions =
      Firestore.instance.collection('sessions');
  saveTutor(Tutor tutor) {
    try {
      tutors.document(tutor.uid).setData(
            tutor.toJson(),
          );
    } catch (e) {
      print(e);
    }
  }

  saveSession(Session session, Tutor tutor) {
    try {
      sessions.document().setData(
            session.toJson(tutor.uid),
          );
    } catch (e) {
      print(e);
    }
  }

  Future<Tutor> checkTutor(User tempUser) async {
    try {
      var response = await tutors.document(tempUser.uid).get();
      if (response.data != null) {
        return Tutor.fromJson(response.data);
      }
      return null;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<Session>> fetchTutorSessions(Tutor tutor) async {
    try {
      var response = await sessions
          .where('tutor.uid', isEqualTo: tutor.uid)
          .getDocuments();
      List<Session> tutorSessions = [];
      Session tempSession;
      response.documents.forEach((element) {
        print(element.data['sessionStart']);
        tempSession = Session.fromJson(element.data);
        if (tempSession.sessionStart.isAfter(DateTime.now())) {
          tempSession.tutor = tutor;
          tutorSessions.add(tempSession);
        }
      });
      return tutorSessions;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<Session>> fetchAllSessions() async {
    try {
      var response = await sessions.getDocuments();
      List<Session> allSessions = [];
      response.documents.forEach((element) {
        Session tempSession = Session.fromJson(element.data);
        if (tempSession.sessionStart.isAfter(
          DateTime.now(),
        )) {
          allSessions.add(tempSession);
          print(tempSession.title);
        }
      });
      return allSessions;
    } catch (e) {
      print(e);
      return null;
    }
  }
}

import 'package:bghiteNe9rra/const.dart';
import 'package:bghiteNe9rra/models/category.dart';

class SessionValidator {
  String validatePlatform(String platform) {
    if (platform == null) return 'session_form_platform_error_1';
    if (!supportedPlatforms.contains(platform))
      return 'session_form_platform_error_2';
    return null;
  }

  String validateTitle(String title) {
    String pattern =
        r'^(((\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])|\w|[0-9]|)+\s?)+$';
    RegExp reg = RegExp(pattern);
    if (title == null || title.length < 1) return 'session_form_title_error_1';
    if (title.length > 50) return 'session_form_title_error_2';
    if (title.length < 13) return 'session_form_title_error_3';
    if (!reg.hasMatch(title)) return 'session_form_title_error_4';
    return null;
  }

  String validateAudienceDescription(String audienceDesciption) {
    if (audienceDesciption == null || audienceDesciption.length < 1)
      return 'session_form_audience_error_1';
    if (audienceDesciption.length < 30) return 'session_form_audience_error_2';
    if (audienceDesciption.length > 130) return 'session_form_audience_error_3';
    return null;
  }

  String validateDescription(String description) {
    if (description == null || description.length < 1)
      return 'session_form_description_error_1';
    if (description.length < 90) return 'session_form_description_error_2';
    if (description.length > 250) return 'session_form_description_error_3';
    return null;
  }

  String validTags(List<String> tags) {
    if (tags == null || tags.length < 4) return 'session_form_tags_error_1';
    if (tags.length > 5) return 'session_form_tags_error_2';
    return null;
  }

  String validSessionLink(String link) {
    if (link == null || link.length < 1) return 'session_form_link_error_1';
    if (link.length > 256) return 'session_form_link_error_2';
    if (link.length < 13) return 'session_form_link_error_3';
    return null;
  }

  String validSessionStart(DateTime sessionStart) {
    if (sessionStart == null) return 'session_form_starttime_error_1';
    if (sessionStart.isBefore(DateTime.now().add(Duration(minutes: 10))))
      return 'session_form_starttime_error_2';
    return null;
  }

  String validDuration(Duration duration) {
    if (duration == null) return 'session_form_duration_error_1';
    if (duration.inMinutes > 120) return 'session_form_duration_error_2';
    if (duration.inMinutes < 20) return 'session_form_duration_error_3';
    return null;
  }

  String validCategory(Category category) {
    if (category == null) return 'session_form_category_error_1';
    return null;
  }
}

class TutorValidator {
  String validateProfileLink(String profileLink) {
    String twitterPattern =
        '(?<twitter_profile>(?:https?:)?\/\/(?:[A-z]+\.)?twitter\.com\/@?(?<username>[A-z0-9_]+)\/?)';
    String linkedinPattern =
        '(?<linked_in_user>(?:https?:)?\/\/(?:[\w]+\.)?linkedin\.com\/in\/(?<permalink>[\w\\-\\_À-ÿ%]+)\/?)';
    String facebookPattern =
        '(?<facebook>((?:https?:)?\/\/(?:www\.)?(?:facebook|fb)\.com\/(?<profile>(?![A-z]+\.php)(?!marketplace|gaming|watch|me|messages|help|search|groups)[A-z0-9_\\-\\.]+)\/?)|((?:https?:)?\/\/(?:www\.)facebook.com/(?:profile.php\?id=)?(?<id>[0-9]+)))';
    String instagramPattern =
        '(?<instagram>(?:https?:)?\/\/(?:www\.)?(?:instagram\.com|instagr\.am)\/(?<instagram_username>[A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?))';

    String googlePlusPattern =
        '(?<googlePlus>(?:https?:)?\/\/plus\.google\.com\/\+(?<gplus_username>[A-z0-9+]+)|(?:https?:)?\/\/plus\.google\.com\/(?<gplus_id>[0-9]{21}))';
    String youtubePattern =
        '(?<youtube>((?:https?:)?\/\/(?:[A-z]+\.)?youtube.com\/user\/(?<youtube_username>[A-z0-9]+)\/?)|(?:https?:)?\/\/(?:[A-z]+\.)?youtube.com\/channel\/(?<youtube_id>[A-z0-9-\_]+)\/?)';
    String snapchatPattern =
        '(?:https?:)?\/\/(?:www\.)?snapchat\.com\/add\/(?<snapchat_username>[A-z0-9\.\_\-]+)\/?';
    String pattern =
        '$twitterPattern|$linkedinPattern|$facebookPattern|$instagramPattern|$googlePlusPattern|$youtubePattern|$snapchatPattern';
    RegExp regExp = RegExp(pattern);
    if (profileLink == null || profileLink.length < 1)
      return 'form_tutor_error_1';
    if (profileLink.length > 256) return 'form_tutor_error_2';
    if (!regExp.hasMatch(profileLink)) return 'form_tutor_error_3';
    return null;
  }

  String validateDescription(String description) {
    if (description == null || description.length < 1)
      return 'form_tutor_description_error_1';
    if (description.length < 90) return 'form_tutor_description_error_2';
    if (description.length > 250) return 'form_tutor_description_error_3';
    return null;
  }
}

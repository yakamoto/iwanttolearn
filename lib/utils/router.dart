import 'package:bghiteNe9rra/screens/categories_page.dart';
import 'package:bghiteNe9rra/screens/controller.dart';
import 'package:bghiteNe9rra/screens/home_page.dart';
import 'package:bghiteNe9rra/screens/login.dart';
import 'package:flutter/material.dart';

class CustomRouter {
  static getRoutes(BuildContext context) => {
        '/controller': (context) => Controller(),
        '/home': (context) => HomePage(),
        '/categories': (context) => CategoriesPage(),
        '/login': (context) => LoginScreen(),
      };
}

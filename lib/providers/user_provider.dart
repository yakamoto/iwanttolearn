import 'dart:async';

import 'package:bghiteNe9rra/models/session.dart';
import 'package:bghiteNe9rra/models/tutor.dart';
import 'package:bghiteNe9rra/models/user.dart';
import 'package:bghiteNe9rra/services/firebase_auth.dart';
import 'package:bghiteNe9rra/services/firestore_db.dart';
import 'package:bghiteNe9rra/utils/validator.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class UserProvider extends ChangeNotifier with AuthService, TutorValidator {
  User user;
  Tutor tutor;
  List<Session> tutorSessions = [];
  List<Session> allSessions;
  bool waitingGoogleUser = false;
  bool isTutorValid = false;
  Map<String, String> errors = Map<String, String>();

  String tempProfileLink;
  String tempDescription;

  FirestoreDataBase db = FirestoreDataBase();

  UserProvider() {
    errors['profile_link'] = null;
    errors['description'] = null;
  }
  Locale _appLocale;
  set appLocale(String code) {
    Intl.defaultLocale = code;

    _appLocale = Locale(code);
    notifyListeners();
  }

  get appLocale => _appLocale;

  Future<void> signInAnonym() async {
    user = await signInAnonymously();
    if (allSessions == null) allSessions = await db.fetchAllSessions();
    notifyListeners();
  }

  Future<void> gooSignIn() async {
    waitingGoogleUser = true;
    notifyListeners();
    User tempUser = await googleSignIn();
    if (tempUser != null) {
      tutor = await db.checkTutor(tempUser);
      if (tutor != null) {
        tutor.uid = tempUser.uid;
        tutorSessions = await db.fetchTutorSessions(tutor);
        if (allSessions == null) allSessions = await db.fetchAllSessions();
      }
      logout();
      user = tempUser;
      notifyListeners();
    }
    waitingGoogleUser = false;
    notifyListeners();
  }

  allLogOut() {
    if (tutor != null) {
      tutor = null;
      isTutorValid = false;
      tempDescription = null;
      tempProfileLink = null;
    }
    if (user.isAnonymous) {
      logout();
    } else {
      googleLogout();
    }
  }

  evolveUserToTutor() {
    if (user.isAnonymous) {
      print('cannot promote to tutor');
    } else {
      tutor = Tutor.fromUser(user);
      tutor.description = tempDescription;
      tutor.profileLink = tempProfileLink;
      db.saveTutor(tutor);
    }
  }

  set profileLink(String profileLink) {
    errors['profile_link'] = validateProfileLink(profileLink);
    if (errors['profile_link'] == null)
      tempProfileLink = profileLink;
    else
      tempProfileLink = null;
    checkValidity();
    notifyListeners();
  }

  set description(String description) {
    errors['description'] = validateDescription(description);
    if (errors['description'] == null)
      tempDescription = description;
    else
      tempDescription = null;
    checkValidity();
    notifyListeners();
  }

  checkValidity() {
    isTutorValid =
        (errors['description'] == null) && (errors['profile_link'] == null);
  }
}

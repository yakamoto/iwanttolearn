import 'package:bghiteNe9rra/const.dart' as initial;
import 'package:bghiteNe9rra/models/category.dart';
import 'package:flutter/material.dart';

class CategoryProvider extends ChangeNotifier {
  List<Category> categories;
  CategoryProvider() {
    this.categories = initial.categories;
  }
}

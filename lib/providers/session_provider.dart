import 'package:bghiteNe9rra/const.dart';
import 'package:bghiteNe9rra/models/category.dart';
import 'package:bghiteNe9rra/models/session.dart';
import 'package:bghiteNe9rra/providers/user_provider.dart';
import 'package:bghiteNe9rra/utils/validator.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class SessionProvider extends ChangeNotifier with SessionValidator {
  UserProvider userProvider;
  Session session;
  Map<String, String> errors = Map<String, String>();
  bool isSessionFormValid = false;
  bool fullSessions = false;
  SessionProvider({this.userProvider}) {
    session = Session();
  }

  List<Session> get tutorSessions => userProvider.tutorSessions;
  List<Session> get allSessions => userProvider.allSessions;

  set title(String title) {
    errors['title_error'] = validateTitle(title);
    if (errors['title_error'] == null)
      session.title = title;
    else
      session.title = null;
    notifyListeners();
  }

  get category => session.category;
  set category(Category category) {
    errors['session_category_error'] = validCategory(category);
    if (errors['session_category_error'] == null)
      session.category = category;
    else
      session.category = null;
    notifyListeners();
  }

  get startTime => session.sessionStart != null
      ? DateFormat.yMMMMd().add_jm().format(session.sessionStart)
      : null;
  get rawStartTime => session.sessionStart != null
      ? session.sessionStart
      : DateTime.now().add(Duration(
          minutes: 20,
        ));
  set startTime(DateTime startTime) {
    errors['start_time_error'] = validSessionStart(startTime);
    if (errors['start_time_error'] == null)
      session.sessionStart = startTime;
    else
      session.sessionStart = null;
    notifyListeners();
  }

  set duration(String duration) {
    try {
      Duration tempDuration;
      if (duration.length < 1)
        errors['duration_error'] = 'Duration must be provided';
      else {
        int minutes = int.parse(duration);
        tempDuration = Duration(minutes: minutes);
        errors['duration_error'] = validDuration(tempDuration);
      }
      if (errors['duration_error'] == null)
        session.duration = tempDuration;
      else
        session.duration = null;
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  set sessionLink(String sessionLink) {
    errors['session_link_error'] = validSessionLink(sessionLink);
    if (errors['session_link_error'] == null)
      session.link = sessionLink;
    else
      session.link = null;
    notifyListeners();
  }

  get platform => session.platform;

  set platform(String platform) {
    errors['platform_error'] = validatePlatform(platform);
    if (errors['platform_error'] == null)
      session.platform = platform;
    else
      session.platform = null;
    notifyListeners();
  }

  set audienceDesciption(String description) {
    errors['audience_description_error'] =
        validateAudienceDescription(description);
    if (errors['audience_description_error'] == null)
      session.audienceDescription = description;
    else
      session.audienceDescription = null;
    notifyListeners();
  }

  set sessionDescription(String description) {
    errors['session_description_error'] = validateDescription(description);
    if (errors['session_description_error'] == null)
      session.longDescription = description;
    else
      session.longDescription = null;
    notifyListeners();
  }

  set tags(String tags) {
    List<String> separatedTags = tags.split(';');
    errors['tags_error'] = validTags(separatedTags);
    if (errors['tags_error'] == null)
      session.tags = separatedTags;
    else
      session.tags = null;
    notifyListeners();
  }

  checkQuota() {
    fullSessions = userProvider.tutorSessions.length >= 3;
    if (fullSessions) {
      notifyListeners();
    }
  }

  checkSessionValidity() {
    errors['title_error'] = validateTitle(session.title);
    errors['session_category_error'] = validCategory(session.category);
    errors['start_time_error'] = validSessionStart(session.sessionStart);
    errors['duration_error'] = validDuration(session.duration);
    errors['session_link_error'] = validSessionLink(session.link);
    errors['platform_error'] = validatePlatform(session.platform);
    errors['audience_description_error'] =
        validateAudienceDescription(session.audienceDescription);
    errors['session_description_error'] =
        validateDescription(session.longDescription);
    errors['tags_error'] = validTags(session.tags);

    isSessionFormValid = true;
    for (String key in errors.keys) {
      if (errors[key] != null) {
        isSessionFormValid = false;
        notifyListeners();
      }
    }

    if (isSessionFormValid && userProvider.tutorSessions.length < 3) {
      session.tutor = userProvider.tutor;
      userProvider.db.saveSession(session, userProvider.tutor);
      userProvider.tutorSessions.add(session);
      userProvider.allSessions.add(session);
    }
  }

  List<Category> get categoriesCount {
    Map<String, int> count = {};
    categories.forEach((category) {
      count[category.name] = 0;
    });
    if (userProvider.allSessions != null &&
        userProvider.allSessions.length >= 1) {
      for (Session session in userProvider.allSessions) {
        if (count[session.category.name] != null)
          count[session.category.name] += 1;
      }
      categories.forEach((category) {
        category.sessionCount = count[category.name];
      });
    }
    return categories;
  }

  List<Session> searchByTag(String value) {
    List<Session> validSessions = [];
    for (Session session in userProvider.allSessions) {
      if (session.tags.contains(value.toLowerCase())) {
        validSessions.add(session);
      }
    }
    return validSessions;
  }

  // set plateform(String platform) {
  //   platformError = validatePlatform(platform);
  // }
}

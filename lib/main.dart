import 'package:bghiteNe9rra/providers/category_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'internationalization/app_localization.dart';
import 'providers/session_provider.dart';
import 'providers/user_provider.dart';
import 'theme.dart';
import 'utils/router.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserProvider>(create: (_) => UserProvider()),
        ChangeNotifierProvider<CategoryProvider>(
          create: (_) => CategoryProvider(),
        ),
        ChangeNotifierProxyProvider<UserProvider, SessionProvider>(
          create: (_) => SessionProvider(userProvider: null),
          update: (BuildContext context, UserProvider userProvider,
                  SessionProvider sessionProvider) =>
              SessionProvider(userProvider: userProvider),
        ),
      ],
      child: Consumer<UserProvider>(builder: (_, userProvider, __) {
        return MaterialApp(
          locale: userProvider.appLocale,
          supportedLocales: [
            Locale('en', 'US'),
            Locale('ar', 'MA'),
            Locale('fr', 'FR'),
          ],
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          title: 'BhiteNe9ra',
          routes: CustomRouter.getRoutes(context),
          initialRoute: '/controller',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            primaryColor: CustomTheme.orange,
            visualDensity: VisualDensity.adaptivePlatformDensity,
            textTheme:
                GoogleFonts.josefinSansTextTheme(Theme.of(context).textTheme),
          ),
        );
      }),
    );
  }
}

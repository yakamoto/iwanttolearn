import 'package:flutter/material.dart';

class CustomTheme {
  static Color grey = Color(0xFF484F57);
  static Color orange = Color(0xFFFFAD3F);
  static Color blue = Color(0xFF57BCE5);
}

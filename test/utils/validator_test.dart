import 'package:bghiteNe9rra/models/category.dart';
import 'package:bghiteNe9rra/models/session.dart';
import 'package:bghiteNe9rra/utils/validator.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  Session session = Session();
  SessionValidator validator = SessionValidator();

  group('Session Platform validator tests', () {
    test('Session platform cannot be null', () {
      String result = validator.validatePlatform(session.platform);
      expect(result, 'session_form_platform_error_1');
    });
    test('Session platform must be in supported plaforms', () {
      String result = validator.validatePlatform('Twitter');
      expect(result, 'session_form_platform_error_2');
    });
    test('Session platform is valid', () {
      String result = validator.validatePlatform('Facebook');
      expect(result, null);
    });
  });

  group('Session Title validator tests', () {
    test('Session title is valid', () {
      String result = validator.validateTitle('Python masterclass 🧣');
      expect(result, null);
    });
    test('Session title length must be less than 50 characters', () {
      String result = validator.validateTitle(
          'Python masterclass, but i want to introduce more complex features like oriented python programing, reactivity, test driven developement and more...ant to introduce more complex features like oriented python programing, reactivity, test driven developement and more...ant to introduce more complex features like oriented python programing, reactivity, test driven developement and more...ant to introduce more complex features like oriented python programing, reactivity, test driven developement and more...ant to introduce more complex features like oriented python programing, reactivity, test driven developement and more...');
      expect(result, 'session_form_title_error_2');
    });

    test('Session title length must be greater then 13 characters', () {
      String title = 'a';
      String result = validator.validateTitle(title);
      expect(result, 'session_form_title_error_3');
    });

    // test(
    //     'Session title must only contain characters, numbers, emojis and spaces',
    //     () {
    //   String result = validator.validateTitle('aasdasdasdasdasdads_/(');
    //   expect(result,
    //       'session_form_title_error_4');
    // });
  });

  group('Session Audience descritpion test', () {
    test('Session audience description cannot be null', () {
      String audienceDesciption;
      String result = validator.validateAudienceDescription(audienceDesciption);
      expect(result, 'session_form_audience_error_1');
    });

    test('Session audience description must contain a minimum of 30 characters',
        () {
      String audienceDesciption = 'asd';
      String result = validator.validateAudienceDescription(audienceDesciption);
      expect(result, 'session_form_audience_error_2');
    });
    test(
        'Session audience description must contain a maximum of 130 characters',
        () {
      String audienceDesciption =
          'asdkjaksjdbakjsdbkajsdnkajsdbkajsbdkajbsdkasjbdkajsbdkjabsdkjasbdkjsabkdjabksjdbaksbdkajsdbkasjbdkasjbdkjasbkjdabskjdbakdbkajsbdkjasbkdjabskdbaskjdbaksjbdaksbdkjasbdkabsdkjabsjkdbakjbdajkbdkabsdkjabsdkabdkajsbdkjasbdkbsakdbaksbdkabdkajsbdkjbsdakbjbdkasbdkbadskjabsdkj';
      String result = validator.validateAudienceDescription(audienceDesciption);
      expect(result, 'session_form_audience_error_3');
    });
  });

  group('Session link tests', () {
    test('Session link must be provided', () {
      String link1 = '';
      String link2;
      String result1 = validator.validSessionLink(link1);
      String result2 = validator.validSessionLink(link2);
      expect(result1, 'session_form_link_error_1');
      expect(result2, 'session_form_link_error_1');
    });

    test('Session link should not exceed 256 characters', () {
      String link = 'a' * 300;
      String result = validator.validSessionLink(link);
      expect(result, 'session_form_link_error_2');
    });

    test('Session link should contain at least 13 characters', () {
      String link = 'a';
      String result = validator.validSessionLink(link);
      expect(result, 'session_form_link_error_3');
    });
  });

  group('Session category tests', () {
    test('Session category must be provided', () {
      Category category;
      String result = validator.validCategory(category);
      expect(result, 'session_form_category_error_1');
    });
  });
  group('Session Descritpion tests', () {
    test('Session description cannot be null', () {
      String desciption;
      String result = validator.validateDescription(desciption);
      expect(result, 'session_form_description_error_1');
    });
    test('Session description must contain a minimum of 90 characters', () {
      String desciption = 'asdk';
      String result = validator.validateDescription(desciption);
      expect(result, 'session_form_description_error_2');
    });
    test('Session description must contain a maximum of 250 characters', () {
      String desciption =
          'asdkjaksjdbakjsdbkajsdnkajsdbkajsbdkajbsdkasjbdkajsbdkjabsdkjasbdkjsabkdjabksjdbaksbdkajsdbkasjbdkasjbdkjasbkjdabskjdbakdbkajsbdkjasbkdjabskdbaskjdbaksjbdaksbdkjasbdkabsdkjabsjkdbakjbdajkbdkabsdkjabsdkabdkajsbdkjasbdkbsakdbaksbdkabdkajsbdkjbsdakbjbdkasbdkbadskjabsdkj';
      String result = validator.validateDescription(desciption);
      expect(result, 'session_form_description_error_3');
    });
  });

  group('Session Tags tests', () {
    test('Session tags connot be null', () {
      List<String> tags;
      String result = validator.validTags(tags);
      expect(result, 'session_form_tags_error_1');
    });
    test('Session tags length must be less or equal to 5', () {
      List<String> tags = ['a', 'b', 'c', 'd', 'e', 'd'];
      String result = validator.validTags(tags);
      expect(result, 'session_form_tags_error_2');
    });
  });

  group('Session datetime start tests', () {
    test('Session start time cannot be null', () {
      DateTime sessionStart;
      String result = validator.validSessionStart(sessionStart);
      expect(result, 'session_form_starttime_error_1');
    });

    test('Session start time must be in future', () {
      DateTime sessionStart = DateTime.now().subtract(Duration(days: 1));
      DateTime sessionStartNow = DateTime.now().add(Duration(minutes: 5));
      String result = validator.validSessionStart(sessionStart);
      String resultNow = validator.validSessionStart(sessionStartNow);
      expect(result, 'session_form_starttime_error_2');
      expect(resultNow, 'session_form_starttime_error_2');
    });
  });

  group('Session duration tests', () {
    test('Session Duration cannot be null', () {
      Duration duration;
      String result = validator.validDuration(duration);
      expect(result, 'session_form_duration_error_1');
    });
    test('Session Duration cannot exceed 120 minutes', () {
      Duration duration = Duration(hours: 3);
      String result = validator.validDuration(duration);
      expect(result, 'session_form_duration_error_2');
    });
    test('Session Duration cannot be less than 20 minutes', () {
      Duration duration = Duration(minutes: 1);
      String result = validator.validDuration(duration);
      expect(result, 'session_form_duration_error_3');
    });
  });

  group('Tutor profile tests', () {
    TutorValidator validator = TutorValidator();
    test('Tutor profile could not exceed 256 character', () {
      String profileLink =
          'qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm';
      String result = validator.validateProfileLink(profileLink);
      expect(result, 'form_tutor_error_2');
    });

    test('Tutor must provide a profile link', () {
      String profileLink;
      String profileLink2 = '';
      String result = validator.validateProfileLink(profileLink);
      String result2 = validator.validateProfileLink(profileLink2);

      expect(result, 'form_tutor_error_1');
      expect(result2, 'form_tutor_error_1');
    });

    test('Provide a valid profile link', () {
      String profileLink = 'aji tchouf';
      String result = validator.validateProfileLink(profileLink);
      expect(result, 'form_tutor_error_3');
    });
    test('Provide a facebook profile link', () {
      String profileLink = 'https://www.facebook.com/hamza.alami.543';
      String result = validator.validateProfileLink(profileLink);
      expect(result, null);
    });

    test('Description must be provided', () {
      String description = '';
      String description1;
      String result = validator.validateDescription(description);
      String result1 = validator.validateDescription(description1);
      expect(result, 'form_tutor_description_error_1');
      expect(result1, 'form_tutor_description_error_1');
    });
    test('Description must contain greater or equal to 90 characters', () {
      String description = 'Yay';
      String result = validator.validateDescription(description);
      expect(result, 'form_tutor_description_error_2');
    });
    test('Description must contain less or equal to 250 characters', () {
      String description =
          'YaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjklYaywertyuiopsdfghjklxcvbnmertyuioghjklñasghjkghjkl';
      String result = validator.validateDescription(description);
      expect(result, 'form_tutor_description_error_3');
    });
  });
}
